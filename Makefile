clean:
	@echo "remove previous builds ..."
	@rm -vf build/migrate
	@rm -vf build/server
	@rm -vf build/worker
	@echo

build: clean
	@echo "build binaries ..."
	go build -o build/migrate migrate/*.go
	go build -o build/server main.go
	go build -o build/worker cmd/thumbit/thumbit.go
	@echo

migrate: build
	./build/migrate

start: build
	export NATS_CLIENT_ID=kb-producer ; ./build/server

worker: build
	export NATS_CLIENT_ID=kb-consumer ; ./build/worker

test:
	go test -c repository/*.go -o build/repository.test && ./build/repository.test -test.v

nats-connect:
	kubectl port-forward svc/nats 4222 8222 -n nats

nats-debug:
	natscat -l -s '>'

protoc:
	find pb/ -iname "*.pb.go" -exec rm -fv {} \;
	protoc --proto_path=proto --go_out=pb --go_opt=paths=source_relative proto/**.proto proto/**/*.proto
