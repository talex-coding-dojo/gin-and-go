package model

import (
	"fmt"

	uuid "github.com/satori/go.uuid"
)

type SparePart struct {
	// meta data
	tableName struct{}  `pg:"replacements,alias:r"`
	ID        uuid.UUID `pg:"uuid,type:uuid"`

	// relations
	Kites []Kite `pg:"many2many:kite_parts,joinFK:kite_uuid"`

	// fields
	Name        string `pg:"type:varchar(100),unique,notnull"`
	Description string `pg:"summary,default:'',notnull"`
	Quantity    int    `pg:"default:1,notnull"`
}

func (s SparePart) String() string {
	return fmt.Sprintf("SparePart<UUID=%q Name=%q Quantity=%d>", s.ID, s.Name, s.Quantity)
}

type KitePart struct {
	// meta data
	tableName struct{} `pg:"alias:kp"`

	// relations (join table)
	KiteID      uuid.UUID `pg:"kite_uuid,pk,type:uuid"`
	Kite        *Kite
	SparePartID uuid.UUID `pg:"spare_part_uuid,pk,type:uuid"`
	SparePart   *SparePart
}

func (k KitePart) String() string {
	return fmt.Sprintf("KitePart<KiteUUID=%q SparePartUUID=%q>", k.KiteID, k.SparePartID)
}
