package model

import (
	"fmt"

	uuid "github.com/satori/go.uuid"
)

type Bag struct {
	// meta data
	tableName struct{}  `pg:"alias:b"`
	ID        uuid.UUID `pg:"uuid,type:uuid"`

	// relations
	Kites []*Kite `pg:"joinTable:kites,fk:bag_id"`

	// fields
	Name string `pg:"type:varchar(100),unique,notnull"`
}

func (b Bag) String() string {
	return fmt.Sprintf("Bag<UUID=%q Name=%q>", b.ID, b.Name)
}
