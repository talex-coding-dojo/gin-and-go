package model

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

type BaseKite struct {
	DisplayName      string // TODO: configure json mapping
	ManufacturerName string // TODO: configure json mapping
	ModelName        string // TODO: configure json mapping
	NumLines         int    // TODO: configure json mapping
	Stackable        bool   // TODO: configure json mapping

	Wind string // TODO: configure json mapping
	Type string // TODO: configure json mapping

	AdditionalAttributes map[string]string // TODO: configure json mapping
}
type KiteCreation struct {
	BaseKite

	PictureID uuid.UUID // TODO: configure json mapping
	BagID     uuid.UUID // TODO: configure json mapping

	SpareParts []*BaseSparePart // TODO: configure json mapping
}

type Kite struct {
	BaseKite

	UUID uuid.UUID // TODO: configure json mapping

	Picture    *Picture     // TODO: configure json mapping
	Bag        *SimpleBag   // TODO: configure json mapping
	SpareParts []*SparePart // TODO: configure json mapping

	CreatedAt  time.Time // TODO: configure json mapping
	ModifiedAt time.Time // TODO: configure json mapping
}
