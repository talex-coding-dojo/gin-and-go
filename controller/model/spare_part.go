package model

import (
	uuid "github.com/satori/go.uuid"
)

type BaseSparePart struct {
	Name        string // TODO: configure json mapping
	Description string // TODO: configure json mapping
	Quantity    int    // TODO: configure json mapping
}

type SparePart struct {
	BaseSparePart

	UUID uuid.UUID // TODO: configure json mapping
}
