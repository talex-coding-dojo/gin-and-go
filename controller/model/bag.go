package model

import (
	uuid "github.com/satori/go.uuid"
)

type BaseBag struct {
	Name string // TODO: configure json mapping
}
type BagCreation struct {
	BaseBag
}

type BagModify struct {
	BaseBag
}

type SimpleBag struct {
	BaseBag

	UUID uuid.UUID // TODO: configure json mapping
}

type Bag struct {
	BaseBag

	UUID     uuid.UUID // TODO: configure json mapping
	NumKites int       // TODO: configure json mapping
}
