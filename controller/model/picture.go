package model

import (
	"strings"

	uuid "github.com/satori/go.uuid"
)

type BasePicture struct {
	Description string // TODO: configure json mapping
	Height      int32  // TODO: configure json mapping
	Width       int32  // TODO: configure json mapping
	Data        string // TODO: configure json mapping
}

type PictureCreation struct {
	BasePicture
}

type Picture struct {
	BasePicture

	UUID uuid.UUID // TODO: configure json mapping
}

func (p *BasePicture) MimeType() string {
	parts := strings.Split(p.Data, ";")
	mimeType := strings.Split(parts[0], ":")

	return mimeType[1]
}
