package nats

type MessageType string

const (
	PictureCreatedEvent MessageType = "picture.created"
	DeadLetter          MessageType = "dead.letter"
)
