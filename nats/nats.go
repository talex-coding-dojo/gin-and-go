package nats

import (
	"github.com/nats-io/stan.go"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"

	"gitlab.com/talex-coding-dojo/kite-base/config"
	"gitlab.com/talex-coding-dojo/kite-base/pb"
)

var sc stan.Conn

func InitNATS() {
	var err error

	if !config.App.NATS.Enabled {
		return
	}

	url := config.App.NATS.ServerURL
	natsServer := stan.NatsURL(config.App.NATS.ServerURL)
	connLost := stan.SetConnectionLostHandler(connectionLost)

	clusterID := config.App.NATS.ClusterID
	clientID := config.App.NATS.ClientID + "-" + uuid.NewV4().String()

	sc, err = stan.Connect(clusterID, clientID, natsServer, connLost)
	if err != nil {
		log.WithError(err).Fatal("could not connect NATS")
	}

	log.WithFields(log.Fields{"URL": url, "ClientID": clientID}).Warn("successfully connected NATS")
}

func CloseNATS() {
	if sc == nil {
		return
	}

	err := sc.Close()
	if err != nil {
		log.WithError(err).Error("could not disconnect NATS")
		return
	}

	sc = nil
}

func Publish(messageType string, envelope *pb.Envelope) {
	if sc == nil {
		return
	}

	binary, err := proto.Marshal(envelope)
	if err != nil {
		log.WithError(err).Error("could not serialize event")
		return
	}

	err = sc.Publish(messageType, binary)
	if err != nil {
		log.WithError(err).Error("could not publish event")
		return
	}

	if log.GetLevel() == log.DebugLevel {
		json, err := protojson.Marshal(envelope)
		if err != nil {
			log.WithError(err).Debug("could not serialize event")
			return
		}

		log.WithField("JSON", string(json)).Debugf("published %q event to NATS", messageType)
		return
	}

	switch payload := envelope.Payload.(type) {
	case *pb.Envelope_Event:
		switch event := payload.Event.Event.(type) {
		case *pb.Event_PictureCreated:
			pictureUUID := event.PictureCreated.Id.Value
			fields := log.Fields{"CorrelationId": envelope.CorrelationId, "PictureUUID": pictureUUID}
			log.WithFields(fields).Infof("published %q event to NATS", messageType)
		}
	case *pb.Envelope_DeadLetter:
		fields := log.Fields{"CorrelationId": envelope.CorrelationId, "ErrorMessage": payload.DeadLetter.ErrorMessage}
		log.WithFields(fields).Warn("published dead letter to NATS")
	}
}

func Register(queue string, subject string, name string, handler stan.MsgHandler, options ...stan.SubscriptionOption) (sub stan.Subscription, err error) {
	if sc == nil {
		return
	}

	// support continue with next message after consumer restart
	durableName := stan.DurableName(name)
	options = append(options, durableName)

	// attach to consumer group to allow scale-up of workers
	sub, err = sc.QueueSubscribe(subject, queue, handler, options...)
	if err != nil {
		log.WithError(err).WithField("Queue", queue).Errorf("could not register handler for %q", subject)
		return nil, err
	}

	return sub, nil
}

func connectionLost(_ stan.Conn, err error) {
	log.WithError(err).Fatal("lost connection to NATS")
}
