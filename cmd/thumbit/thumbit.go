package main

import (
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/nats-io/stan.go"
	log "github.com/sirupsen/logrus"

	"gitlab.com/talex-coding-dojo/kite-base/config"
	"gitlab.com/talex-coding-dojo/kite-base/handler"
	"gitlab.com/talex-coding-dojo/kite-base/middleware"
	"gitlab.com/talex-coding-dojo/kite-base/nats"
	"gitlab.com/talex-coding-dojo/kite-base/repository"
)

func init() {
	// Init connection to NATS streaming server
	nats.InitNATS()
}

func main() {
	// Init logger
	middleware.InitLogrus()

	// Start interrupt handler
	trapInterrupts()

	// Start timer
	start := time.Now()

	// Init database connection
	repository.InitDB()

	// Register handler
	queue := config.App.NATS.SubscriptionGroup
	subject := string(nats.PictureCreatedEvent)
	myName := "thumbit"

	subscription, err := nats.Register(queue, subject, myName, handler.PictureCreatedHandler, stan.SetManualAckMode())
	if err != nil {
		log.WithError(err).Fatal("could not complete worker startup")
	}

	if !subscription.IsValid() {
		log.WithError(err).Fatal("invalid NATS listener")
	}

	log.WithFields(log.Fields{"Queue": queue, "Subject": subject, "DurableName": myName}).Warn("registered NATS event handler")

	// Stop timer
	duration := getDurationInMillis(start)
	log.WithField("Latency", duration).Info("worker startup completed")

	// Begin infinite consumer loop
	for {
		select {}
	}
}

func trapInterrupts() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)

	go func() {
		sig := <-sigs

		nats.CloseNATS()
		repository.CloseDB()

		log.WithField("Signal", sig).Warn("exited worker")
		os.Exit(1)
	}()
}

func getDurationInMillis(start time.Time) float64 {
	end := time.Now()
	duration := end.Sub(start)

	milliseconds := float64(duration) / float64(time.Millisecond)
	rounded := float64(int(milliseconds*100+.5)) / 100

	return rounded
}
