module gitlab.com/talex-coding-dojo/kite-base

go 1.14

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/gin-gonic/gin v1.6.3
	github.com/go-pg/pg/v10 v10.0.0-beta.5
	github.com/go-playground/validator/v10 v10.4.1 // indirect
	github.com/golang/protobuf v1.4.3
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/nats-io/jwt v1.1.0 // indirect
	github.com/nats-io/nats-server/v2 v2.1.8 // indirect
	github.com/nats-io/nats-streaming-server v0.18.0 // indirect
	github.com/nats-io/nkeys v0.2.0 // indirect
	github.com/nats-io/stan.go v0.7.0
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/sakirsensoy/genv v1.0.1
	github.com/satori/go.uuid v1.2.0
	github.com/segmentio/encoding v0.2.0 // indirect
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.6.1
	github.com/ugorji/go v1.1.13 // indirect
	github.com/vmihailenco/msgpack/v5 v5.0.0-beta.8 // indirect
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897 // indirect
	golang.org/x/exp v0.0.0-20200821190819-94841d0725da // indirect
	golang.org/x/net v0.0.0-20201026091529-146b70c837a4 // indirect
	golang.org/x/sys v0.0.0-20201026133411-418715ba6fdd // indirect
	google.golang.org/grpc v1.33.1 // indirect
	google.golang.org/protobuf v1.25.0
	gopkg.in/yaml.v2 v2.3.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200605160147-a5ece683394c // indirect
)
