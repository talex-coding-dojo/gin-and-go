package mapper

import (
	dto "gitlab.com/talex-coding-dojo/kite-base/controller/model"
	db "gitlab.com/talex-coding-dojo/kite-base/repository/model"
)

type BagMapper struct{}

func GetBagMapper() *BagMapper {
	return &BagMapper{}
}

func (m *BagMapper) ToDB(dtoBag *dto.Bag) (dbBag *db.Bag, err error) {
	dbBag = &db.Bag{
		ID:    dtoBag.UUID,
		Name:  dtoBag.Name,
		Kites: nil,
	}

	return dbBag, nil
}

func (m *BagMapper) ToDTO(dbBag *db.Bag) (dtoBag *dto.Bag, err error) {
	dtoBag = &dto.Bag{
		UUID: dbBag.ID,
		BaseBag: dto.BaseBag{
			Name: dbBag.Name,
		},
	}

	if dbBag.Kites != nil {
		dtoBag.NumKites = len(dbBag.Kites)
	}

	return dtoBag, nil
}
