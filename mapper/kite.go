package mapper

import (
	dto "gitlab.com/talex-coding-dojo/kite-base/controller/model"
	db "gitlab.com/talex-coding-dojo/kite-base/repository/model"
)

type KiteMapper struct {
	bagMapper     *BagMapper
	pictureMapper *PictureMapper
}

func GetKiteMapper() *KiteMapper {
	return &KiteMapper{
		bagMapper:     GetBagMapper(),
		pictureMapper: GetPictureMapper(),
	}
}

func (m *KiteMapper) ToDB(dtoKite *dto.Kite) (dbKite *db.Kite, err error) {
	dbKite = &db.Kite{
		ID:                   dtoKite.UUID,
		PictureID:            dtoKite.Picture.UUID,
		BagID:                dtoKite.Bag.UUID,
		DisplayName:          dtoKite.DisplayName,
		ManufacturerName:     dtoKite.ManufacturerName,
		ModelName:            dtoKite.ModelName,
		NumLines:             dtoKite.NumLines,
		Stackable:            dtoKite.Stackable,
		Wind:                 db.WindLevel(dtoKite.Wind),
		Type:                 db.ConstructionType(dtoKite.Type),
		AdditionalAttributes: dtoKite.AdditionalAttributes,
		CreatedAt:            dtoKite.CreatedAt,
		ModifiedAt:           dtoKite.ModifiedAt,
		Bag: &db.Bag{
			ID:   dtoKite.Bag.UUID,
			Name: dtoKite.Bag.Name,
		},
	}

	dbKite.Picture, err = m.pictureMapper.ToDB(dtoKite.Picture)
	if err != nil {
		return nil, err
	}

	for _, sparePart := range dtoKite.SpareParts {
		dbSparePart := &db.SparePart{
			ID:          sparePart.UUID,
			Name:        sparePart.Name,
			Description: sparePart.Description,
			Quantity:    sparePart.Quantity,
		}

		dbKite.SpareParts = append(dbKite.SpareParts, dbSparePart)
	}

	return dbKite, nil
}

func (m *KiteMapper) ToDTO(dbKite *db.Kite) (dtoKite *dto.Kite, err error) {
	dtoKite = &dto.Kite{
		BaseKite: dto.BaseKite{
			DisplayName:          dbKite.DisplayName,
			ManufacturerName:     dbKite.ManufacturerName,
			ModelName:            dbKite.ModelName,
			NumLines:             dbKite.NumLines,
			Stackable:            dbKite.Stackable,
			Wind:                 string(dbKite.Wind),
			Type:                 string(dbKite.Type),
			AdditionalAttributes: dbKite.AdditionalAttributes,
		},
		Bag: &dto.SimpleBag{
			BaseBag: dto.BaseBag{Name: dbKite.Bag.Name},
			UUID:    dbKite.Bag.ID,
		},
		UUID:       dbKite.ID,
		CreatedAt:  dbKite.CreatedAt,
		ModifiedAt: dbKite.ModifiedAt,
	}

	dtoKite.Picture, err = m.pictureMapper.ToDTO(dbKite.Picture)
	if err != nil {
		return nil, err
	}

	for _, dbSparePart := range dbKite.SpareParts {
		dtoSparePart := &dto.SparePart{
			BaseSparePart: dto.BaseSparePart{
				Name:        dbSparePart.Name,
				Description: dbSparePart.Description,
				Quantity:    dbSparePart.Quantity,
			},
			UUID: dbSparePart.ID,
		}

		dtoKite.SpareParts = append(dtoKite.SpareParts, dtoSparePart)
	}

	return dtoKite, nil
}
