package mapper

import (
	"strings"

	dto "gitlab.com/talex-coding-dojo/kite-base/controller/model"
	db "gitlab.com/talex-coding-dojo/kite-base/repository/model"
)

type PictureMapper struct{}

func GetPictureMapper() *PictureMapper {
	return &PictureMapper{}
}

func (m *PictureMapper) ToDB(dtoPicture *dto.Picture) (dbPicture *db.Picture, err error) {
	parts := strings.Split(dtoPicture.Data, ";")
	mimeType := strings.Split(parts[0], ":")

	dbPicture = &db.Picture{
		ID:          dtoPicture.UUID,
		Description: dtoPicture.Description,
		MimeType:    mimeType[1],
		Height:      dtoPicture.Height,
		Width:       dtoPicture.Width,
		Data:        dtoPicture.Data,
	}

	return dbPicture, nil
}

func (m *PictureMapper) ToDTO(dbPicture *db.Picture) (dtoPicture *dto.Picture, err error) {
	dtoPicture = &dto.Picture{
		UUID: dbPicture.ID,
		BasePicture: dto.BasePicture{
			Description: dbPicture.Description,
			Height:      dbPicture.Height,
			Width:       dbPicture.Width,
			Data:        dbPicture.Data,
		},
	}

	return dtoPicture, nil
}
