package service

import (
	"bytes"
	"encoding/base64"
	"image"
	"image/jpeg"
	"image/png"
	"strings"

	"github.com/nfnt/resize"

	dto "gitlab.com/talex-coding-dojo/kite-base/controller/model"
)

func (s *PictureService) CreateThumbnail(sourcePicture *dto.Picture) (thumbnailPicture *dto.Picture, err error) {
	var (
		img    image.Image
		prefix string
	)

	parts := strings.Split(sourcePicture.Data, ",")
	data := parts[1]

	switch sourcePicture.MimeType() {
	case "image/jpeg":
		img, err = jpeg.Decode(base64.NewDecoder(base64.StdEncoding, strings.NewReader(data)))
		if err != nil {
			return nil, err
		}
		prefix = "data:image/jpeg;base64,"
	case "image/png":
		img, err = png.Decode(base64.NewDecoder(base64.StdEncoding, strings.NewReader(data)))
		if err != nil {
			return nil, err
		}
		prefix = "data:image/png;base64,"
	}

	imgResized := resize.Resize(96, 96, img, resize.Bilinear)

	buf := new(bytes.Buffer)
	err = jpeg.Encode(buf, imgResized, nil)
	if err != nil {
		return nil, err
	}

	content := buf.Bytes()

	encoded := base64.StdEncoding.EncodeToString(content)

	thumbnailPicture = &dto.Picture{
		BasePicture: dto.BasePicture{
			Description: sourcePicture.Description,
			Height:      int32(imgResized.Bounds().Max.Y),
			Width:       int32(imgResized.Bounds().Max.X),
			Data:        prefix + encoded,
		},
		UUID: sourcePicture.UUID,
	}

	return thumbnailPicture, err
}
