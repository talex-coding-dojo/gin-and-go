package service

import (
	uuid "github.com/satori/go.uuid"

	dto "gitlab.com/talex-coding-dojo/kite-base/controller/model"
	"gitlab.com/talex-coding-dojo/kite-base/mapper"
	"gitlab.com/talex-coding-dojo/kite-base/repository"
	db "gitlab.com/talex-coding-dojo/kite-base/repository/model"
)

type BagService struct {
	repository *repository.BagRepository
	mapper     *mapper.BagMapper
}

func GetBagServiceInstance() *BagService {
	return &BagService{
		repository: repository.GetBagRepository(),
		mapper:     mapper.GetBagMapper(),
	}
}

func (s *BagService) Create(data *dto.BagCreation) (bag *dto.Bag, err error) {
	dbBag := &db.Bag{
		ID:   uuid.NewV4(),
		Name: data.Name,
	}

	err = s.repository.Store(dbBag)
	if err == nil {
		bag, err = s.mapper.ToDTO(dbBag)
	}

	return bag, err
}

func (s *BagService) FindOneByUUID(uid uuid.UUID) (bag *dto.Bag, err error) {
	dbBag, err := s.repository.FetchByID(uid)
	if err == nil {
		bag, err = s.mapper.ToDTO(dbBag)
	}

	return bag, err
}

func (s *BagService) FindAll() (bags []*dto.Bag, err error) {
	dbBags, err := s.repository.FetchAll()
	if err == nil {
		bags, err = s.mapSliceOfBags(dbBags)
	}

	return bags, err
}

func (s *BagService) FindAllByName(name string) (bags []*dto.Bag, err error) {
	dbBags, err := s.repository.FetchAllByNameLike(name)
	if err == nil {
		bags, err = s.mapSliceOfBags(dbBags)
	}

	return bags, err
}

func (s *BagService) Update(id uuid.UUID, data *dto.BagModify) (bag *dto.Bag, err error) {
	dbBag := &db.Bag{
		ID:   id,
		Name: data.Name,
	}

	err = s.repository.Update(dbBag)
	if err == nil {
		bag, err = s.mapper.ToDTO(dbBag)
	}

	return bag, err
}

func (s *BagService) Destroy(uid uuid.UUID) (err error) {
	dbBag, err := s.repository.FetchByID(uid)
	if err == nil {
		return s.repository.Delete(dbBag)
	}

	return err
}

func (s *BagService) mapSliceOfBags(dbBags []*db.Bag) (bags []*dto.Bag, err error) {
	for _, dbBag := range dbBags {
		bag, err := s.mapper.ToDTO(dbBag)
		if err != nil {
			return nil, err
		}

		bags = append(bags, bag)
	}

	return bags, nil
}
