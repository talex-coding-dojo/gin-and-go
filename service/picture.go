package service

import (
	"encoding/base64"
	"image"
	"image/jpeg"
	"image/png"
	"strings"

	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"

	"gitlab.com/talex-coding-dojo/kite-base/mapper"
	"gitlab.com/talex-coding-dojo/kite-base/nats"
	"gitlab.com/talex-coding-dojo/kite-base/repository"

	dto "gitlab.com/talex-coding-dojo/kite-base/controller/model"
	db "gitlab.com/talex-coding-dojo/kite-base/repository/model"
)

type PictureService struct {
	repository *repository.PictureRepository
	mapper     *mapper.PictureMapper
}

func GetPictureServiceInstance() *PictureService {
	return &PictureService{
		repository: repository.GetPictureRepository(),
		mapper:     mapper.GetPictureMapper(),
	}
}

func (s *PictureService) Create(data *dto.PictureCreation) (picture *dto.Picture, err error) {
	defer func() {
		proto := mapper.GetProtoMapper().ToPictureCreated(picture)
		nats.Publish(string(nats.PictureCreatedEvent), proto)
	}()

	dbPicture := &db.Picture{
		ID:          uuid.NewV4(),
		Description: data.Description,
		MimeType:    data.MimeType(),
		Height:      data.Height,
		Width:       data.Width,
		Data:        data.Data,
	}

	err = s.repository.Store(dbPicture)
	if err == nil {
		picture, err = s.mapper.ToDTO(dbPicture)
	}

	return picture, err
}

func (s *PictureService) FindOneByUUID(uid uuid.UUID) (picture *dto.Picture, err error) {
	dbPicture, err := s.repository.FetchByID(uid)
	if err == nil {
		picture, err = s.mapper.ToDTO(dbPicture)
	}

	return picture, err
}

func (s *PictureService) RenderImageByUUID(uid uuid.UUID) (picture []byte, mimeType string, err error) {
	dbPicture, err := s.repository.FetchByID(uid)
	if err != nil {
		return nil, "", err
	}

	parts := strings.Split(dbPicture.Data, ",")
	data := parts[1]

	var cfg image.Config

	switch dbPicture.MimeType {
	case "image/jpeg":
		cfg, err = jpeg.DecodeConfig(base64.NewDecoder(base64.StdEncoding, strings.NewReader(data)))
		if err != nil {
			return nil, "", err
		}
	case "image/png":
		cfg, err = png.DecodeConfig(base64.NewDecoder(base64.StdEncoding, strings.NewReader(data)))
		if err != nil {
			return nil, "", err
		}
	}

	fields := log.Fields{"Width": cfg.Width, "Height": cfg.Height, "MimeType": dbPicture.MimeType}
	log.WithFields(fields).Debug("decoded base64 image")

	picture, err = base64.StdEncoding.DecodeString(data)

	return picture, dbPicture.MimeType, err
}

func (s *PictureService) FindAll() (pictures []*dto.Picture, err error) {
	dbPictures, err := s.repository.FetchAll()
	if err == nil {
		pictures, err = s.mapSliceOfPictures(dbPictures)
	}

	return pictures, err
}

func (s *PictureService) Destroy(uid uuid.UUID) (err error) {
	dbPicture, err := s.repository.FetchByID(uid)
	if err == nil {
		return s.repository.Delete(dbPicture)
	}

	return err
}

func (s *PictureService) mapSliceOfPictures(dbPictures []*db.Picture) (pictures []*dto.Picture, err error) {
	for _, dbPicture := range dbPictures {
		picture, err := s.mapper.ToDTO(dbPicture)
		if err != nil {
			return nil, err
		}

		pictures = append(pictures, picture)
	}

	return pictures, nil
}
