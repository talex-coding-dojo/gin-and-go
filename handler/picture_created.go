package handler

import (
	"fmt"
	"time"

	"github.com/golang/protobuf/proto"
	"github.com/nats-io/stan.go"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"

	"gitlab.com/talex-coding-dojo/kite-base/config"
	"gitlab.com/talex-coding-dojo/kite-base/mapper"
	"gitlab.com/talex-coding-dojo/kite-base/nats"
	"gitlab.com/talex-coding-dojo/kite-base/pb"
	"gitlab.com/talex-coding-dojo/kite-base/repository"
	"gitlab.com/talex-coding-dojo/kite-base/service"
)

func PictureCreatedHandler(m *stan.Msg) {
	var (
		logEntry = log.WithFields(log.Fields{"Subject": m.Subject, "RedeliveryCount": m.RedeliveryCount})

		success = false

		envelope pb.Envelope
		err      error
	)

	defer func(start time.Time) {
		duration := getDurationInMillis(start)
		log.WithFields(log.Fields{"Latency": duration, "Handler": "PictureCreated"}).Debugf("handler completed after %f ms", duration)
	}(time.Now())

	defer func() {
		if success {
			err = m.Ack()
			if err != nil {
				logEntry.WithError(err).Errorf("could not acknowledge %q event", m.Subject)
			}

			logEntry.Debugf("acknowledged %q event", m.Subject)
			return
		}

		msg := recover().(string)

		if config.App.NATS.EnableDeadLetters && m.Redelivered {
			if m.RedeliveryCount >= config.App.NATS.MaxRedeliveryCount-1 {
				deadLetter := mapper.GetProtoMapper().BuildDeadLetter(&envelope, msg)
				nats.Publish(string(nats.DeadLetter), deadLetter)

				err = m.Ack()
				if err != nil {
					logEntry.WithError(err).Errorf("could not acknowledge %q event, published dead letter", m.Subject)
				}

				logEntry.Debugf("acknowledged %q event, published dead letter", m.Subject)
			}
		}
	}()

	err = proto.Unmarshal(m.Data, &envelope)
	if err != nil {
		logEntry.WithError(err).Errorf("could not deserialize %q event", m.Subject)
		panic(err.Error())
	}

	logEntry = logEntry.WithField("CorrelationId", envelope.CorrelationId)

	switch payload := envelope.Payload.(type) {
	case *pb.Envelope_Event:
		switch event := payload.Event.Event.(type) {
		case *pb.Event_PictureCreated:
			pictureUUID := event.PictureCreated.Id.Value
			mediaFile := event.PictureCreated.GetPicture()

			logEntry = logEntry.WithField("PictureUUID", pictureUUID)

			dtoPicture, err := mapper.GetProtoMapper().FromPictureCreated(uuid.NewV4().String(), mediaFile)
			if err != nil {
				logEntry.WithError(err).Errorf("could not transform %q event", m.Subject)
				panic(err.Error())
			}

			tnPicture, err := service.GetPictureServiceInstance().CreateThumbnail(dtoPicture)
			if err != nil {
				logEntry.WithError(err).Error("could not render thumbnail picture")
				panic(err.Error())
			}

			dbPicture, err := mapper.GetPictureMapper().ToDB(tnPicture)
			if err != nil {
				logEntry.WithError(err).Error("could not transform DTO to DB model")
				panic(err.Error())
			}

			err = repository.GetPictureRepository().Store(dbPicture)
			if err != nil {
				logEntry.WithError(err).Error("could not store thumbnail picture")
				panic(err.Error())
			}

			// TODO: emit ThumbnailRendered event

			logEntry.WithField("ThumbnailUUID", dbPicture.ID).Info("processing successful, created new thumbnail")

			success = true
		default:
			logEntry.WithField("Event", fmt.Sprintf("%v", event)).Error("event not supported")
		}
	default:
		logEntry.WithField("Envelope", fmt.Sprintf("%v", &envelope)).Error("payload not supported")
	}
}

func getDurationInMillis(start time.Time) float64 {
	end := time.Now()
	duration := end.Sub(start)

	milliseconds := float64(duration) / float64(time.Millisecond)
	rounded := float64(int(milliseconds*100+.5)) / 100

	return rounded
}
