package middleware

import (
	"os"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/talex-coding-dojo/kite-base/config"
)

func InitLogrus() {
	if "json" == strings.ToLower(config.App.Logging.Format) {
		log.SetFormatter(&log.JSONFormatter{})
	} else {
		log.SetFormatter(&log.TextFormatter{})
	}

	log.SetOutput(os.Stdout)

	level, _ := log.ParseLevel(config.App.Logging.Level)
	log.SetLevel(level)

	fields := log.Fields{"Format": config.App.Logging.Format, "Level": log.GetLevel()}
	log.WithFields(fields).Warn("configured Logrus logger")
}
