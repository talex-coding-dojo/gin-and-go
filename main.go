package main

import (
	_ "github.com/sakirsensoy/genv/dotenv/autoload"

	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"

	"gitlab.com/talex-coding-dojo/kite-base/config"
	"gitlab.com/talex-coding-dojo/kite-base/middleware"
	"gitlab.com/talex-coding-dojo/kite-base/nats"
	"gitlab.com/talex-coding-dojo/kite-base/repository"
)

func init() {
	// Init connection to NATS streaming server
	nats.InitNATS()
}

func main() {
	var (
		router *gin.Engine
	)

	// Init logger
	middleware.InitLogrus()

	// Start interrupt handler
	trapInterrupts()

	// Start timer
	start := time.Now()

	// Init database connection
	repository.InitDB()

	// TODO: Config Gin Router

	// Stop timer
	duration := getDurationInMillis(start)
	log.WithField("Latency", duration).Info("application startup completed")

	// Start server
	svrAddress := fmt.Sprintf("%s:%d", config.App.IpAddress, config.App.Port)
	fields := log.Fields{"ServerAddress": svrAddress, "ProductionMode": config.App.ProductionMode}
	log.WithFields(fields).Warn("running server")

	// TODO: Run Gin Router

	if router == nil {
		// nothing to do
	}

	log.WithFields(fields).Warn("stopped server")
}

func trapInterrupts() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)

	go func() {
		sig := <-sigs

		repository.CloseDB()

		log.WithField("Signal", sig).Warn("exited server")
		os.Exit(1)
	}()
}

func getDurationInMillis(start time.Time) float64 {
	end := time.Now()
	duration := end.Sub(start)

	milliseconds := float64(duration) / float64(time.Millisecond)
	rounded := float64(int(milliseconds*100+.5)) / 100

	return rounded
}
